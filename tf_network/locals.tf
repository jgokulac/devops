locals {
  env = "${terraform.workspace == "default" ? "dev" : terraform.workspace}"

  vpc_cidrs = {
    "dev"  = "10.11.64.0/18"
    "prod" = "10.11.192.0/18"
  }

  private_subnets_map = {
    "dev"  = ["10.11.64.0/21", "10.11.72.0/21"]
    "prod" = ["10.11.192.0/21", "10.11.200.0/21"]
  }

  public_subnets_map = {
    "dev"  = ["10.11.80.0/21", "10.11.88.0/21"]
    "prod" = ["10.11.208.0/21", "10.11.216.0/21"]
  }

  database_subnets_map = {
    "dev"  = ["10.11.96.0/21", "10.11.104.0/21"]
    "prod" = ["10.11.224.0/21", "10.11.232.0/21"]
  }

  env_sshpubkey_file = {
    "dev"  = "${var.dev_sshpubkey_file}"
    "prod" = "${var.prod_sshpubkey_file}"
  }

  vpc_cidr         = "${local.vpc_cidrs[local.env]}"
  private_subnets  = "${local.private_subnets_map[local.env]}"
  public_subnets   = "${local.public_subnets_map[local.env]}"
  database_subnets = "${local.database_subnets_map[local.env]}"
  sshpubkey_file   = "${local.env_sshpubkey_file[local.env]}"

  common_tags = {
    "Terraform"   = "true"
    "Environment" = "${local.env}"
  }
}
